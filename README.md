this uses [jekyll](https://atom.io/packages/jekyll) for site generation, so look at that documentation if you want to know more.

---

## how to test it locally

This section isn't totally necessary, but it could be helpful to know if you want to see how your changes will look before you commit them to the repository.

#### dependencies to install:

* [ruby](https://www.ruby-lang.org) - the language that jekyll is built in
* [bundler](http://bundler.io/) - allows you to easily install the dependencies

#### steps:

1. clone repository to wherever you want
1. open up a command prompt
1. enter repository's directory and run `bundle install`
1. use `jekyll serve`, which will generate the website into `_site/`
1. nagivate to `localhost:4000/` in your web browser
    * the site should theoretically automatically regenerate whenever you make changes, so just refresh your browser
    
tbh using a local nginx instance pointed to `_site/` and using `jekyll build --watch` is probably better though, since webrick doesn't have unicode support for *some reason*
