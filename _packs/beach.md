---
price: 0
files:
    - beach.ball
    - beach.bucket.capped
    - beach.chair.blue
    - beach.chair.green
    - beach.chair.red
    - beach.dock
    - beach.land
    - beach.land.mega
    - beach.land.quarter
    - beach.palmtree
    - beach.table
    - beach.towel
    - beach.toyshovel
    - beach.umbrella
    - beach.wall.big
    - beach.water
    - beach.water.quarter
    
---
beach pack sun not included


