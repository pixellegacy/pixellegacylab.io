---
price: 0
files:
    - cabinet.chibi
    - cabinet.jump
    - cabinet.tag
    - cabinet.zombie
    - cabinet.zoom
    - vend.battleship
    - vend.bomb.dirt
    - vend.bomb.fish
    - vend.bomb.heal
    - vend.bomb.ice
    - vend.bomb.mega
    - vend.bomb
    - vend.bomb.vacu
    - vend.bzanbato
    - vend.caps
    - vend.chess
    - vend.hpenergy
    - vend.shield
    - vend.sword
    - vend.team.fist
    - vend.team.guardian
    - vend.team.healtar
    - vend.team.helm
    - vend.team.shield
    - vend.team.smiter
    - vend.team.zanbato
    - vend.testhat
    - vend.testsword

---
cabinets and fun arcade things
