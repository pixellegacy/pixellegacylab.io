---
price: 0
files:
    - robot.antenna.blue
    - robot.antenna.red
    - robot.arm.blue
    - robot.chest.blue
    - robot.chest.red
    - robot.finger.blue
    - robot.foot.blue
    - robot.forearm.blue
    - robot.hand.blue
    - robot.head.blue
    - robot.head.red
    - robot.leg.blue
    - robot.leg.blue2
    - robot.optic.green
    - robot.optic.red
    - robot.optic.small.green
    - robot.optic.small.red
    - robot.waist.blue

---
a pack to construct robots
