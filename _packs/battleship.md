---
price: 0
files:
    - battleship.battleship
    - battleship.carrier
    - battleship.cruiser
    - battleship.carrier
    - battleship.hit
    - battleship.letters
    - battleship.miss
    - battleship.numbers
    - battleship.patrol
    - battleship.sub
    - battleship.wall
    
---
fun pack to play battleship yes