---
price: 0
files:
    - shrine.build
    - shrine.elite
    - shrine.faux
    - shrine.frozen
    - shrine.locked
    - shrine
    - shrine.safe
    - shrine.static
    - shrine.team

---
a meta pack to contain all of the shrines