---
price: 0
files:
    - chess.bishop.black
    - chess.bishop.white
    - chess.board
    - chess.king.black
    - chess.king.white
    - chess.knight.black
    - chess.knight.white
    - chess.land
    - chess.pawn.black
    - chess.pawn.white
    - chess.queen.black
    - chess.queen.white
    - chess.rook.black
    - chess.rook.white
    
---
hello yes let's play chess