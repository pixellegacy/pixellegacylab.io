---
price: 0
files:
    - pipe.4way
    - pipe.5way
    - pipe.6way
    - pipe.corner
    - pipe.end
    - pipe.land
    - pipe.lid
    - pipe.mega
    - pipe.short
    - pipe.tall
    - pipe.tsection
    - pipe.wall.blue
    - pipe.wall.green
    - pipe.wall.old
    - pipe.wall.red
    - pipe.water

---
a pack for pipes obviously
