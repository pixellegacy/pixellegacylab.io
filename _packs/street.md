---
price: 0
files:
    - street.asphalt.mega
    - street.asphalt
    - street.firehydrant.burst
    - street.firehydrant
    - street.highway.2way.cross
    - street.highway.3way.cross
    - street.highway.4way.cross
    - street.highway.4way.pipe
    - street.highway.4way
    - street.highway.corner
    - street.highway.crosswalk
    - street.highway.double
    - street.highway.mega
    - street.highway.pipe
    - street.highway
    - street.lamp.big
    - street.lamp.fancy
    - street.lamp
    - street.light.left
    - street.light
    - street.pipelid
    - street.road
    - street.road.quarter
    - street.sidewalk.corner
    - street.sidewalk.mega
    - street.sidewalk
    - street.sidewalk.quarter
    - street.sign.arrow.left
    - street.sign.arrow.right
    - street.sign.heart
    - street.sign.stop
    - street.sign.x
    - street.telephonepoles
    - street.trafficcone

---
roads, signs, and traffic cones
