

module Jekyll
    module SubstringFilter
        def substring(input, start_at, end_at = nil)
          end_at = -1 if end_at.nil?
          input[start_at.to_i..end_at.to_i]
        end
    end
end

Liquid::Template.register_filter(Jekyll::SubstringFilter)