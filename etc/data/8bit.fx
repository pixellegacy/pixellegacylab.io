float4x4 World            : WORLD;
float4x3 matWorldIT : WORLDINVERSETRANSPOSE;
//float4x4 matWorldViewProj : WORLDVIEWPROJECTION;
float4x4 CamView          : VIEW;
float4x4 CamProj          : PROJECTION;
float4x4 LightView;
float4x4 LightProj;
float4x3 BoneMatrices[52] : BONES;
int      iNumBonePerVertex: BONESPERVERTEX;
float    ShadowAmount     = 0.15f;
float	 darkness		  = 0.0f;
float    Bias             = 0.001f;
texture  Diffuse          : TEXTURE0;
texture  Depth;
float4 materialDiffuse : DIFFUSE;
float4 materialEmissive : EMISSIVE;
float4 subtractor : SPECULAR;
float4 LightC = float4(.3,.5,1,1);

sampler2D DiffuseSample = sampler_state
{
  Texture   = (Diffuse);
  AddressU = Wrap;
  AddressV = Wrap;
};

sampler DepthSample = sampler_state
{
	Texture   = (Depth);
	AddressU = Border;
	AddressV = Border;
	AddressW = Border;
	BorderColor = 0x000000;
};

struct a2vskinned
{
    float4 Position    : POSITION0;   
    float3 Normal      : NORMAL;
    float2 UV          : TEXCOORD0;  
    float4 Pos         : TEXCOORD1;  
	float4 BlendIndices: BLENDINDICES;
	float4 BlendWeights: BLENDWEIGHT;
};

struct a2v
{
    float4 Position: POSITION0; 
   	float3 Normal  : NORMAL;		
    float2 UV      : TEXCOORD0;  
    float4 Pos     : TEXCOORD1;  
};

struct v2f
{
	float4 Position: POSITION0;
    float3 Normal  : TEXCOORD2;
	float2 UV      : TEXCOORD0;  
    float4 Pos     : TEXCOORD1;  
};

struct sv2f
{
	float4 Position: POSITION;
	float  Depth   : TEXCOORD0;
};

void TPN(uniform int iNumBones, float4 ModelPos, float3 ModelNorm, float4 BoneWeights, float4 fBoneIndices, out float3 Pos, out float3 Norm)
{
	if(iNumBones == 0)
	{
		Pos                 = mul(ModelPos, World);
		Norm                = mul(ModelNorm, World);
	}
	else
	{
		int4  iBone         = D3DCOLORtoUBYTE4(fBoneIndices);
		int   IndexArray[4] = (int[4])iBone;
		      Pos           = 0;
		      Norm          = 0;
		float LastWeight    = 0;
		
		for(int i = 0; i < iNumBones - 1; i++)
		{
			float4x3 Mat = BoneMatrices[IndexArray[i]];
			LastWeight   = LastWeight + BoneWeights[i];
			Pos         += mul(ModelPos, Mat) * BoneWeights[i];
			Norm        += mul(ModelNorm, (float3x3)Mat) * BoneWeights[i];
		}
		
		LastWeight         = 1 - LastWeight;
		float4x3 Mat       = BoneMatrices[IndexArray[iNumBones-1]];
		         Pos      += mul(ModelPos, Mat) * LastWeight;
		         Norm     += mul(ModelNorm, (float3x3)Mat) * LastWeight;
	}
	return;
}

sv2f skinvpdeep(uniform int iNumBones, in a2vskinned IN)
{
	sv2f Output;
	float3 Pos, Norm;
	
	TPN(iNumBones, IN.Position, IN.Normal, IN.BlendWeights, IN.BlendIndices, Pos, Norm);
	Output.Position = mul(float4(Pos, 1), mul(LightView, LightProj));
	Output.Depth.r  = 1-(Output.Position.z / Output.Position.w);
	
	return Output;	
}

v2f skinvp(uniform int iNumBones, in a2vskinned IN)
{
	v2f Output;
	float3 Pos, Norm;
	
	TPN(iNumBones, IN.Position, IN.Normal, IN.BlendWeights, IN.BlendIndices, Pos, Norm);
	Output.Position = mul(float4(Pos, 1), mul(CamView, CamProj));
	Output.UV       = IN.UV;
	Output.Pos      = IN.Position;
	float3 n = normalize(Norm);
	Output.Normal   = n;  
	return Output;	
}

sv2f svp(in a2v IN)
{
	sv2f Output;
	float4x4 wvp    = mul(mul(World, LightView), LightProj);	
	Output.Position = mul(IN.Position, wvp);
	Output.Depth.r  = 1-(Output.Position.z / Output.Position.w);
	return Output;
}

float4 fpdeep(in sv2f IN): COLOR0
{
	return float4(IN.Depth.r, 0, 0, 1);
}

float4 sfp(in sv2f IN): COLOR0
{
	return float4(IN.Depth.r, 0, 0, 1);
}

v2f vp(in a2v IN)
{
	v2f Output;
	float4x4 wvp    = mul(mul(World, CamView), CamProj);
	Output.Position = mul(IN.Position, wvp);
	Output.Pos      = IN.Position;
	Output.UV       = IN.UV;
	Output.Normal   = mul(IN.Normal, World);
	return Output;
}

v2f vpLow(in a2v IN)
{
	v2f Output;
	Output.UV       = IN.UV;
	return Output;
}


float4 fp(in v2f IN): COLOR0
{
	float4x4 swvp      = mul(mul(World, LightView), LightProj);
	float4   LP        = mul(IN.Pos, swvp);
	
	float2   SUV       = 0.5f * LP.xy / LP.w + float2(0.5f, 0.5f);
	SUV.y              = 1 - SUV.y;
	
	float    Deep      = tex2D(DepthSample, SUV).r;
	float    PixelDeep = 1-(LP.z / LP.w);
	
	float4 Pixel = materialDiffuse * tex2D(DiffuseSample, IN.UV); 
	
	float a = Pixel.a; 
    float shadow = 0;
	
	if (Deep - Bias >= PixelDeep)
		{
     			shadow = 2;
		}

	Pixel += (dot(-IN.Normal, float3(0,-1,0)) * (LightC * 0.2)) * 0.3;   //(diffuse,Pixel.a) * 0.5;

		
	float darken = max(0, dot(float3(0,1,0), -IN.Normal) * 0.3);

	float shade = min(ShadowAmount*2, max(ShadowAmount ,dot(float3(0,-1,0), -IN.Normal) - shadow));
	
	Pixel += shade - darken - ShadowAmount*2;
	Pixel.r -= darkness;
	Pixel.g -= darkness; 
	Pixel.b += darkness;
	
	Pixel += materialEmissive - subtractor;
			
	Pixel.a = a;
	return Pixel;
}


// Vertex Shader
//float4 VSocc(in a2v IN) : UV {
      //mul(inPosition, matWorldViewProj);
	// v2f Output;
//	Output.UV       = IN.UV;
//   return IN.UV;//Output
//}
 
// Pixel Shader
float4 PSocc(in v2f IN) : COLOR {
	float4 Pixel = materialDiffuse * tex2D(DiffuseSample, IN.UV); 
	float a = Pixel.a;
	Pixel = (Pixel - darkness - subtractor) + materialEmissive;
	Pixel.a = a;
	//Pixel.r -= darkness;
	//Pixel.g -= darkness;
	//Pixel.b += darkness;
    return Pixel;
}
 

technique LowMesh {
    pass P0 {
        VertexShader = compile vs_1_1 vp();
        PixelShader  = compile ps_1_1 PSocc();
    }
}

VertexShader LVSArray[5] = {compile vs_1_1 skinvp(0), compile vs_1_1 skinvp(1), compile vs_1_1 skinvp(2), compile vs_1_1 skinvp(3), compile vs_1_1 skinvp(4)};
technique LowActor
{
    pass pass0
    {
    ShadeMode = Flat;
       VertexShader = (LVSArray[iNumBonePerVertex]);
       PixelShader  = compile ps_1_1 PSocc();
    }
}

technique renderdeepmesh
{
	pass P0
	{
	ShadeMode = Flat;
        VertexShader = compile vs_2_0 svp();
        PixelShader  = compile ps_2_0 sfp(); 
	}
}

technique rendermesh
{
	pass P0
	{
	ShadeMode = Flat;
        VertexShader = compile vs_2_0 vp();
        PixelShader  = compile ps_2_0 fp(); 
	}
}

VertexShader VSDeepArray[5] = {compile vs_2_0 skinvpdeep(0), compile vs_2_0 skinvpdeep(1), compile vs_2_0 skinvpdeep(2), compile vs_2_0 skinvpdeep(3), compile vs_2_0 skinvpdeep(4)};
technique renderdeepactor
{
    pass pass0
    {
	ShadeMode = Flat;
       VertexShader = (VSDeepArray[iNumBonePerVertex]);
       PixelShader  = compile ps_2_0 fpdeep();
    }
}

VertexShader VSArray[5] = {compile vs_2_0 skinvp(0), compile vs_2_0 skinvp(1), compile vs_2_0 skinvp(2), compile vs_2_0 skinvp(3), compile vs_2_0 skinvp(4)};
technique renderactor
{
    pass pass0
    {
    ShadeMode = Flat;
       VertexShader = (VSArray[iNumBonePerVertex]);
       PixelShader  = compile ps_2_0 fp();
    }
}