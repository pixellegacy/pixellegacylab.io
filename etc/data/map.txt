﻿
StartData
-16:-1:Elite:Eternal Radio HQ
-16:0:WeekBuildB:0
-16:1:WeekBuildB:0
-16:2:WeekBuildB:0
-15:-1:WeekBuildB:0
-15:0:Locked:Eternal Heights
-15:1:Locked:Eternal Heights
-15:2:WeekBuildA:0
-14:-1:WeekBuildA:0
-14:0:Locked:Eternal Heights
-14:1:Locked:Eternal Heights
-14:2:Construct:0
-13:-1:WeekBuildA:0
-13:0:Construct:2
-13:1:Normal:0
-13:2:WeekBuildB:0
-12:1:Construct:0
-12:6:Normal:0
-12:7:Locked:GrandmothersHouse
-11:1:Construct:0
-11:5:WeekBuildB:0
-11:6:Locked:Great Forest
-11:7:Locked:Great Forest
-11:8:Locked:Great Forest
-11:9:Locked:FishMine
-11:10:Locked:FishMine
-10:0:WeekBuildA:0
-10:1:WeekBuildA:1
-10:2:Construct:0
-10:3:Construct:0
-10:4:Locked:2nd Sword Dungeon
-10:5:WeekBuildA:0
-10:6:Locked:Great Forest
-10:7:Locked:Great Forest
-10:8:Locked:Great Forest
-10:9:Locked:FishMine Entrance
-10:10:Locked:FishMine
-9:-9:Locked:Ludus Castle
-9:-8:Locked:Ludus Castle
-9:-2:Locked:Token Or Death
-9:-1:WeekBuildB:0
-9:0:WeekBuildA:0
-9:1:Locked:Panic!
-9:2:Locked:Old Cave
-9:3:Elite:The Grand Finale
-9:4:Construct:2
-9:5:WeekBuildA:0
-9:6:Locked:Great Forest
-9:7:Locked:Great Forest
-9:8:Locked:Great Forest
-9:9:WeekBuildA:0
-8:-9:Locked:Ludus Castle
-8:-8:Locked:Ludus Castle
-8:-3:WeekBuildB:2
-8:-2:WeekBuildA:0
-8:-1:Construct:0
-8:0:Locked:Cartridge Subway
-8:1:Safe:Safe Castle
-8:2:Safe:Safe Castle
-8:3:Locked:Secret Forest
-8:4:WeekBuildA:0
-8:5:WeekBuildB:3
-8:6:WeekBuildA:1
-8:7:Locked:Great Forest
-8:8:Construct:2
-8:9:WeekBuildA:0
-7:-3:Locked:Beach
-7:-2:Locked:Beach
-7:-1:Locked:Tronic Beach
-7:0:Locked:Cartridge Road
-7:1:Safe:Safe Castle
-7:2:Safe:Safe Castle
-7:3:Locked:Great Forest Road
-7:4:WeekBuildA:3
-7:5:WeekBuildA:3
-7:6:Construct:2
-7:7:Construct:2
-6:-6:Locked:Business Square
-6:-5:Locked:Cartridge
-6:-4:Sacred:Cartridge
-6:-3:Locked:Cartridge / Beach
-6:-2:Locked:Beach \ BlockGame
-6:-1:Construct:2
-6:0:Elite:C:
-6:1:Public_:0
-6:2:Locked:The Blipfield
-6:3:WeekBuildB:3
-6:4:WeekBuildA:3
-6:5:WeekBuildB:2
-6:6:WeekBuildB:3
-6:7:WeekBuildA:3
-5:-6:Locked:Cartridge - Tower
-5:-5:Sacred:Cartridge
-5:-4:Locked:Cartridge
-5:-3:WeekBuildB:0
-5:-2:WeekBuildB:3
-5:-1:Sacred:Cloud Nine
-5:0:FrozenElite:0
-5:1:Normal:0
-5:2:Normal:0
-5:3:Construct:0
-5:4:Construct:1
-5:5:WeekBuildA:3
-5:6:WeekBuildB:3
-5:7:Locked:Ghost Maze
-4:-6:Locked:Teh Facotry
-4:-5:Locked:Cartridge-Hall
-4:-4:Locked:Cartridge-Arena
-4:-3:Construct:0
-4:-2:WeekBuildA:2
-4:-1:Elite:Spite
-4:0:WeekBuildA:0
-4:1:WeekBuildB:0
-4:2:Public_:0
-4:3:Normal:0
-4:4:WeekBuildB:3
-4:5:WeekBuildA:3
-4:6:Construct:0
-4:7:WeekBuildA:2
-4:8:WeekBuildA:2
-3:-6:Elite:MonochromeDomain
-3:-5:WeekBuildB:0
-3:-4:WeekBuildA:0
-3:-3:WeekBuildA:3
-3:-2:WeekBuildA:3
-3:-1:WeekBuildB:0
-3:0:WeekBuildA:3
-3:1:Construct:2
-3:2:WeekBuildB:3
-3:3:WeekBuildA:0
-3:4:Construct:0
-3:5:WeekBuildA:3
-3:6:WeekBuildB:3
-3:7:WeekBuildA:2
-3:8:WeekBuildB:0
-2:-6:WeekBuildB:3
-2:-5:WeekBuildB:3
-2:-4:WeekBuildB:3
-2:-3:WeekBuildB:3
-2:-2:WeekBuildB:3
-2:-1:WeekBuildB:3
-2:0:WeekBuildA:0
-2:1:WeekBuildB:0
-2:2:WeekBuildA:0
-2:3:WeekBuildA:3
-2:4:Locked:Noob Castle Ruins
-2:5:Locked:Noob Castle Ruins
-2:6:WeekBuildB:0
-2:7:Construct:2
-2:8:WeekBuildB:0
-1:-5:WeekBuildB:3
-1:-4:Locked:Doom Caverns
-1:-3:Locked:Doom Caverns
-1:-2:WeekBuildA:3
-1:-1:Construct:2
-1:0:Construct:1
-1:1:WeekBuildA:1
-1:2:WeekBuildA:0
-1:3:WeekBuildB:0
-1:4:WeekBuildA:0
-1:5:Construct:0
-1:6:WeekBuildA:3
-1:7:WeekBuildB:3
0:-5:WeekBuildA:3
0:-4:Locked:Doom Caverns
0:-3:Locked:Doom Caverns
0:-2:WeekBuildA:3
0:-1:WeekBuildA:0
0:0:Elite:The Origin
0:1:WeekBuildB:0
0:2:Construct:0
0:3:WeekBuildA:0
0:4:WeekBuildB:0
0:5:WeekBuildB:3
0:6:Construct:0
0:7:WeekBuildB:2
1:-5:Construct:2
1:-4:Locked:TOMMAYTEMPLE
1:-3:WeekBuildA:0
1:-2:WeekBuildB:2
1:-1:Locked:Waterfall
1:0:WeekBuildA:1
1:1:WeekBuildA:1
1:2:Locked:
1:3:WeekBuildA:0
1:4:WeekBuildB:0
1:5:WeekBuildA:0
1:6:WeekBuildA:0
1:7:Locked:
1:8:Locked:
2:-5:Construct:0
2:-4:WeekBuildB:0
2:-3:WeekBuildB:2
2:-2:Construct:0
2:0:Construct:2
2:1:WeekBuildB:3
2:2:WeekBuildA:0
2:3:WeekBuildB:0
2:4:WeekBuildA:0
2:5:WeekBuildA:3
2:6:WeekBuildB:3
2:7:Locked:
2:8:Locked:
2:9:Construct:2
3:-4:WeekBuildA:2
3:-3:Locked:Closed For Now.
3:-2:Construct:2
3:-1:Locked:Ice Zone
3:0:Locked:Ice Zone
3:1:Locked:Ice Zone - Shop
3:2:WeekBuildB:0
3:3:WeekBuildA:0
3:4:WeekBuildA:0
3:5:WeekBuildB:0
3:6:Locked:
3:7:Locked:
3:8:Locked:
3:9:Elite:Worship Chromium!
3:10:Locked:
4:-3:Construct:0
4:-2:WeekBuildA:2
4:-1:Locked:Ice Zone - Lodge
4:0:Locked:Ice Zone - Bridge
4:1:Locked:Ice Zone
4:2:WeekBuildB:2
4:3:WeekBuildA:3
4:4:WeekBuildA:1
4:5:WeekBuildB:0
4:6:WeekBuildA:0
4:7:WeekBuildB:2
4:8:WeekBuildB:0
4:9:WeekBuildB:0
5:0:Locked:Ice Tower
5:2:WeekBuildA:3
5:3:WeekBuildB:2
6:2:WeekBuildB:2
20:20:Normal:0
