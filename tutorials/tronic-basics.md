---
author: xelivous
title: Tronic Basics
link: tronic basics
weight: 0
---
Tronics are the in-game visual programming language of Neverdaunt: 8bit.
In this video I will primarily teach you all about the basics of tronics.

Now first you need to learn two terms; Flow, and Data.
These are the main concepts in all tronics, and they are what connect them all together.

Flow is essentially the order that tronics get triggered.
It starts at a "trigger" tronic, and then makes it way through all of the tronics in the order you specify.

Meanwhile Data holds content inside of it. For example it could hold the number 1, or something like "COWS". Quite simply, it's "Data".

Now you can connect every single tronic through the use of their Nodes. The nodes are the little cubes
Each node has a specific function. They almost always follow a specific set of rules.
But the ones that don't will be gone through in more detail in their respective videos.

The basic rules of tronics are the following:
FLOW is always dictated by the RED and YELLOW nodes.
Where RED means output, and YELLOW means input.

It's similar with DATA, as it uses BLUE and GREEN.
BLUE means write, and GREEN means read.

Now connecting the nodes is actually very simple if you keep those previous rules in mind.
All you do is click on them, one by one, and they will automatically attach themselves.

There are a few rules however:
For GREEN/BLUE data nodes, you can only write/read to a single data block. 
But a data block can have as many connections as you need.

For RED flow nodes, you can only flow to a single YELLOW node.
Yet a YELLOW node can accept as many RED as you would like.

You can also go one step further and loop tronics, although it's currently capped at 100 iterations.
All you have to do is wire the output back into an existing input.

Now there are also 3 different kinds of tronics; TRIGGER, COM, and TECH.
TRIGGER tronic initiate the flow. Some common examples of TRIGGERs are; keyboards, buttons, and proximities.
COM tronics usually affect data in one way or another. Some examples of them are; math tronics, ifEquals, and Random.
TECH are special tronics that have specific application. For example; Movers, Rotors, and Radios, and Displays.